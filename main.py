#from __future__ import print_function
#from __future__ import unicode_literals
#from __future__ import division
#from __future__ import absolute_import
#from future import standard_library
#standard_library.install_aliases()

import kivy
kivy.require('1.8.0') # replace with your current kivy version !


__version__ = '1.0.0'
from kivy.app import App
from kivy.uix.label import Label


def main():
    pass


class MyApp(App):
    def build(self):
        return Label(text='Hello world')


if __name__ == '__main__':
    MyApp().run()
